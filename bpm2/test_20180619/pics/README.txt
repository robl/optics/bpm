Beamviewer (d20-cam2) snapshots
-------------------------------

- lamp : no beam, lamp on.

- 2mA_mirror_slits: first acquisition with typical configuration : first mirror + slits.

- 2mA_slits: direct beam (first mirror moved out of the way). slits still in same position as 2mA_mirror_slits.

- 2mA_full_open: no mirror, slits opened at max width.