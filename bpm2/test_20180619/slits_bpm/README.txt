Scans of the BPM2 with 2mA beam
-------------------------------

2018-06-19

- csv files: scan data (slit position and bpm counter value)

- png files: snapshot of the slit position vs counter plot

- optics-2018-06-19.dat and *optics-2018-06-19.h5*: full spec session (not really useful)

- bpm2_20180619.h5: relevant scans extracted out of optics-2018-06-19.dat