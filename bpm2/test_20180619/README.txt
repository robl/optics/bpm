Beamviewer/BPM test with 2mA beam
---------------------------------

Folders :

- pics/: snapshots of the beam viewer with and without mirror and slits.

- bpm_slits/: plots/data of scans performed with the BPM2 (bpm on the edge of the s2 blades).

- mics/: misc. data